package za.co.pjpdev.gone.sprites;

import java.util.ArrayList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;

import za.co.pjpdev.gone.AssetManager;
import za.co.pjpdev.gone.Main;
import za.co.pjpdev.gone.SpriteManager;
import za.co.pjpdev.gone.states.GameplayState;

public class Player extends Sprite {
	
	public int health = 100;
	private int lives = 3;
	
	private float speed = 0.3f;
	private int direction = 0; //0 = north, 1 = west, 2 = south, 3 = east
	
	/* Collision variables */
	public boolean doorCollide = false;
	public boolean intelCollide = false;
	
	/* Other stuff */
	public float progress = 100.0f;

	public Player(String name) throws SlickException {
		super(name);
		
		Image img = AssetManager.IMAGE_PLAYER;
		this.setImage(img);
		
		boundingBox = new Rectangle(x, y, img.getWidth(), img.getHeight());
	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, SpriteManager manager, int delta) throws SlickException {
		/* Update the player */
		Input in = gc.getInput(); //Get the input manager
		float transX = 0;
		float transY = 0;
		
		/* Input */
		if (in.isKeyDown(Input.KEY_UP)) {
			y -= speed * delta;
			transY -= speed * delta;
			direction = 0;
		} else if (in.isKeyDown(Input.KEY_DOWN)) {
			y += speed * delta;
			transY += speed * delta;
			direction = 2;
		} else if (in.isKeyDown(Input.KEY_LEFT)) {
			x -= speed * delta;
			transX -= speed * delta;
			direction = 1;
		} else if (in.isKeyDown(Input.KEY_RIGHT)) {
			x += speed * delta;
			transX += speed * delta;
			direction = 3;
		}
		
		if (in.isKeyPressed(Input.KEY_Z)) {
			manager.addSprite(new Bullet("bb", direction, x + (getImage().getWidth()/2), y + (getImage().getHeight()/2)));
		}
		
		if (in.isKeyPressed(Input.KEY_H))
			health -= 1;
		
		boundingBox.setLocation(x, y);
		
		GameplayState state = (GameplayState) game.getState(Main.STATE_GAMEPLAY);
		Rectangle viewPort = state.viewPort;
		
		/* Collision */
		// Level
		ArrayList<Rectangle> boxes = state.levelManager.getBoxes();
		for (Rectangle rect : boxes) {
			if (boundingBox.intersects(rect)) {
				System.out.println("Collision!");
				if (direction == 0) {
					y += 2;
					transY += 2;
				} else if (direction == 1) {
					x += 2;
					transX += 2;
				} else if (direction == 2) {
					y -= 2;
					transY -= 2;
				} else if (direction == 3) {
					x -= 2;
					transX -= 2;
				} 
			}
		}
		
		// Other Sprites
		ArrayList<Sprite> sprites = manager.getSprites();
		for (Sprite sprite : sprites) {
			if (boundingBox.intersects(sprite.boundingBox)){
				if (sprite.getName().equals("DOOR")) {
					doorCollide = true;
					Door dr = (Door) sprite;
					
					if (boundingBox.intersects(dr.collideBox)) {
						if (direction == 0) {
							y += 2;
							transY += 2;
						} else if (direction == 1) {
							x += 2;
							transX += 2;
						} else if (direction == 2) {
							y -= 2;
							transY -= 2;
						} else if (direction == 3) {
							x -= 2;
							transX -= 2;
						}
					}
					
					if (in.isKeyDown(Input.KEY_X)) {
						dr.unlock(delta);
					}
					
					progress = (dr.value / dr.MAX_VALUE) * 100;
				} else if (sprite.getName().equals("INTEL")) {
					doorCollide = true;
					
					Intel intel = (Intel) sprite;
					
					if (in.isKeyDown(Input.KEY_X)) {
						intel.value -= 0.001f * delta;
					}
					
					progress = (intel.value / intel.MAX_VALUE) * 100;
				} else if (sprite.getName().equals("ESCAPE")) {
					doorCollide = false;
					if (state.intel.isStolen()) {
						game.enterState(Main.STATE_SUCCESS);
					}
				} else {
					doorCollide = false;
				}
			}
		}
		
		viewPort.setX(viewPort.getX() + transX);
		viewPort.setY(viewPort.getY() + transY);
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, SpriteManager manager, Graphics g) throws SlickException {
		/* Render the sprite */
		g.drawImage(getImage(), x, y);
		
		if (Main.DEBUG_RENDER) {
			g.draw(boundingBox);
			g.drawString("X: " + boundingBox.getX() + " Y: " + boundingBox.getY(), boundingBox.getX(), boundingBox.getY());
		}
	}
}
