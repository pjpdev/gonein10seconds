package za.co.pjpdev.gone;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import za.co.pjpdev.gone.sprites.Door;
import za.co.pjpdev.gone.sprites.EscapePortal;
import za.co.pjpdev.gone.sprites.Player;
import za.co.pjpdev.gone.sprites.Sprite;
import za.co.pjpdev.gone.states.GameplayState;

public class LevelManager {
	
	private ArrayList<Image> tileList;
	private ArrayList<Vector2f> positions;
	
	private ArrayList<Rectangle> boxes;
	
	private SpriteManager spriteManager;
	private GameplayState state;

	public LevelManager(GameplayState state) {
		/* Create the manager */
		tileList = new ArrayList<Image>();
		positions = new ArrayList<Vector2f>();
		
		boxes = new ArrayList<Rectangle>();
		
		this.state = state;
		this.spriteManager = state.spriteManager;
	}
	
	public void loadLevel(String level) {
		/* Load level data from the level text file */
		try {
			Scanner scan = new Scanner(new FileInputStream("./assets/levels/" + level + ".txt"));
			
			int row = 0;
			
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				parseLine(line, row);
				
				row += 1;
			}
			
			scan.close();
			
		} catch (Exception ex) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	private void parseLine(String line, int row) throws SlickException {
		for (int i = 0; i < line.length(); i++) {
			char c = line.charAt(i);
			if (c == '#') {
				tileList.add(AssetManager.IMAGE_WALL01);
				boxes.add(new Rectangle(i * 64, row * 64, 64 ,64));
			}
			if (c == '*') {
				tileList.add(AssetManager.IMAGE_FLOOR01);
			}
			if (c == 'P') {
				tileList.add(AssetManager.IMAGE_FLOOR01); // Floor under the player.
				state.player.x = i * 64;
				state.player.y = row * 64;
			}
			if (c == '$') {
				tileList.add(AssetManager.IMAGE_FLOOR01); // Floor under the loot;
				state.intel.x = i * 64;
				state.intel.y = row * 64;
			}
			if (c == 'T') {
				tileList.add(AssetManager.IMAGE_FLOOR01); // Floor under turret;
			}
			if (c == '0') {
				tileList.add(AssetManager.IMAGE_FLOOR01); // Floor under door;
				Door door = new Door("DOOR");
				door.x = i * 64;
				door.y = row *64;
				spriteManager.addSprite(door);
			}
			if (c == '@') {
				tileList.add(AssetManager.IMAGE_ESCAPE); // Floor under exit;
				EscapePortal esc = new EscapePortal("ESCAPE");
				esc.x = i * 64;
				esc.y = row * 64;
				spriteManager.addSprite(esc);
			}
			if (c == ' ') {
				tileList.add(new Image(64, 64)); //Blank space
			}
			positions.add(new Vector2f(i * 64, row * 64));
		}
	}
	
	public void clearLevel() {
		tileList = new ArrayList<Image>();
		positions = new ArrayList<Vector2f>();
		
		boxes = new ArrayList<Rectangle>();
	}
	
	public void update(GameContainer gc, StateBasedGame game, int delta) {
		/* update */
	}
	
	public void render(GameContainer gc, StateBasedGame game, Graphics g) {
		/* render */
		for (int i = 0; i < tileList.size(); i++) {
			Image img = tileList.get(i);
			float x = positions.get(i).x;
			float y = positions.get(i).y;
			g.drawImage(img, x, y);
		}
		
		if (Main.DEBUG_RENDER) {
			for (int i = 0; i < boxes.size(); i++) {
				Rectangle box = boxes.get(i);
				g.draw(box);
				g.drawString("X: " + box.getX() + " Y: " + box.getY(), box.getX(), box.getY());
			}
		}
	}
	
	public ArrayList<Rectangle> getBoxes() {
		return boxes;
	}

}
