package za.co.pjpdev.gone.states;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Music;
import org.newdawn.slick.ShapeFill;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import za.co.pjpdev.gone.AssetManager;
import za.co.pjpdev.gone.LevelManager;
import za.co.pjpdev.gone.Main;
import za.co.pjpdev.gone.SpriteManager;
import za.co.pjpdev.gone.sprites.Intel;
import za.co.pjpdev.gone.sprites.Player;

public class GameplayState extends BasicGameState {
	
	private int id = -1;
	
	public static final int MAX_LEVELS = 3;
	
	public SpriteManager spriteManager;
	public LevelManager levelManager;
	
	public Player player;
	public Intel intel;
	
	public Rectangle viewPort;
	
	/* GUI Elements */
	private Rectangle lifeBar;
	private Rectangle lifeBarBack;
	private Rectangle progressBar;
	private Font timerFont;
	
	/* Gameplaye elements */
	private float timer = 60;
	private float stimer = 1.0f; //sound
	
	private int currentLevel = 1;
	
	/* Audio */
	private Sound timerSound;
	private Sound loseSound;
	private Music normalSong;
	private Music stolenSong;

	public GameplayState(int id) throws SlickException {
		this.id = id;
		
		spriteManager = new SpriteManager();
		
		player = new Player("PLAYER");
		intel = new Intel("INTEL");
	}

	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		/* Initialise the state */
		levelManager = new LevelManager(this);
		
		setLevel();
		
		/* GUI Elements */
		lifeBar = new Rectangle(10, 10, player.health, 15);
		lifeBarBack = new Rectangle(10, 10, player.health, 15);
		progressBar = new Rectangle(270, 290, 100, 25);
		
		timerFont = new TrueTypeFont(new java.awt.Font("Arial", java.awt.Font.BOLD, 24), true);
		
		/* Audio */
		timerSound = AssetManager.loadSound("timer.wav");
		loseSound = AssetManager.loadSound("lose.wav");
		normalSong = AssetManager.loadMusic("normal.ogg");
		normalSong.setVolume(0.05f);
		stolenSong = AssetManager.loadMusic("stolen.ogg");
	}
	
	public void setLevel() {
		spriteManager.clear();
		spriteManager.addSprite(player);
		spriteManager.addSprite(intel);
		
		player.health = 100;
		intel.reset();
		
		timer = 60.0f;
		
		levelManager.clearLevel();
		levelManager.loadLevel("level" + currentLevel);
		
		System.out.println(spriteManager.getSprites().size());
		
		viewPort = new Rectangle(player.x -304, player.y -208, 640, 480);
	}
	
	public void nextLevel(StateBasedGame game) {
		if (currentLevel < MAX_LEVELS) {
			currentLevel += 1;
			game.enterState(Main.STATE_GAMEPLAY);
		} else {
			currentLevel = 1;
			game.enterState(Main.STATE_WINNER);
		}
		
		setLevel();
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		/* Update state elements */
		g.translate(-viewPort.getX(), -viewPort.getY());
		
		levelManager.render(container, game, g);
		spriteManager.render(container, game, g);
		
		/* Render GUI */
		g.translate(viewPort.getX(), viewPort.getY());
		g.setColor(Color.red);
		g.fill(lifeBarBack);
		g.setColor(Color.green);
		g.fill(lifeBar);
		
		if (player.doorCollide) {
			g.drawString("Hold X to interact!", 120, 10);
			g.fill(progressBar);
		}
		
		g.setFont(timerFont);
		g.drawString(String.format("%.0f", timer), 600, 10);
		g.setFont(container.getDefaultFont());
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		/* Render state elements */
		spriteManager.update(container, game, delta);
		
		lifeBar.setWidth(player.health);
		progressBar.setWidth(player.progress);
		
		timer -= 0.001f * delta;
		if (timer <= 0.0f) {
			loseSound.play();
			game.enterState(Main.STATE_FAILED);
		}
		
		if (intel.isStolen()) {
			if (timer > 10) 
				timer = 10.0f;
			stimer -= 0.001f * delta;
			
			if (stimer <= 0) {
				timerSound.play();
				stimer = 1.0f;
			}
			
			if (!stolenSong.playing()) {
				normalSong.stop();
				stolenSong.loop();
				stolenSong.setVolume(0.25f);
			}
		}
	}
	
	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException {
		super.enter(container, game);
		
		setLevel();
		
		normalSong.loop();
		normalSong.setVolume(0.25f);
	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return id;
	}

}
