package za.co.pjpdev.gone.sprites;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;

import za.co.pjpdev.gone.AssetManager;
import za.co.pjpdev.gone.Main;
import za.co.pjpdev.gone.SpriteManager;

public class Door extends Sprite {
	
	public static final float MAX_VALUE = 2.5f;
	
	public Rectangle collideBox;
	
	public float value = 2.5f;

	public Door(String name) {
		super(name);
		
		this.setImage(AssetManager.IMAGE_DOOR);
		boundingBox = new Rectangle(this.x, this.y, 64, 64);
		collideBox = new Rectangle(x+2, y+2, 60, 60);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, SpriteManager manager, int delta) throws SlickException {
		/* Update */
		boundingBox.setLocation(x, y);
		collideBox.setLocation(x+1, y+1);
		
		if ((value < 2.5f) && (value > 0.0f)) {
			value += 0.0001f * delta;
		} else if (value < 0.0f) {
			manager.removeSprite(this);
		}
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, SpriteManager manager, Graphics g) throws SlickException {
		/* Render */
		g.drawImage(getImage(), x, y);
		
		if (Main.DEBUG_RENDER) {
			g.draw(boundingBox);
			g.draw(collideBox);
		}
	}
	
	public void unlock(int delta) {
		value -= 0.001f * delta;
	}

}
