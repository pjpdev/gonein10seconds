package za.co.pjpdev.gone.states;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import za.co.pjpdev.gone.AssetManager;
import za.co.pjpdev.gone.Main;

public class LevelFail extends BasicGameState {
	
	private Image background;
	
	private int id = -1;

	public LevelFail(int id) {
		this.id = id;
	}

	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		/* Initialise */
		background = AssetManager.loadImage("fail_bg.png");
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		/* Render */
		g.drawImage(background, 0, 0);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		/* Update */
		
		Input in = container.getInput();
		
		System.out.println("FUUU...");
		
		if (in.isKeyDown(Input.KEY_X)) {
			GameplayState gstate = (GameplayState) game.getState(Main.STATE_GAMEPLAY);
			gstate.setLevel();
			game.enterState(Main.STATE_GAMEPLAY);
		}
		
		if (in.isKeyPressed(Input.KEY_ESCAPE)) {
			container.exit();
		}
	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return id;
	}

}
