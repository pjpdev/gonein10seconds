package za.co.pjpdev.gone;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import za.co.pjpdev.gone.states.GameplayState;
import za.co.pjpdev.gone.states.LevelFail;
import za.co.pjpdev.gone.states.LevelFinished;
import za.co.pjpdev.gone.states.MenuState;
import za.co.pjpdev.gone.states.Winner;

public class Main extends StateBasedGame {
	
	public static final int STATE_MENU = 0;
	public static final int STATE_GAMEPLAY = 1;
	public static final int STATE_FAILED = 2;
	public static final int STATE_SUCCESS = 3;
	public static final int STATE_WINNER = 4;
	
	public static boolean DEBUG_RENDER = false;

	public Main() {
		super("Gone in 10 Seconds | Ludum Dare 27 Entry");
	}

	@Override
	public void initStatesList(GameContainer container) throws SlickException {
		System.out.println("Initialising States List");
		/* Add States */
		addState(new MenuState(STATE_MENU));
		addState(new GameplayState(STATE_GAMEPLAY));
		addState(new LevelFail(STATE_FAILED));
		addState(new LevelFinished(STATE_SUCCESS));
		addState(new Winner(STATE_WINNER));
		
		/* Enter MenuState */
		enterState(STATE_MENU);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/* Create Main Game Class */
		try {
			AppGameContainer app = new AppGameContainer(new Main());
			app.setDisplayMode(640, 480, false);
			app.setShowFPS(false);
			app.start();
		} catch (SlickException ex) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

}
