package za.co.pjpdev.gone.sprites;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;

import za.co.pjpdev.gone.AssetManager;
import za.co.pjpdev.gone.SpriteManager;

public class Bullet extends Sprite {
	
	private float speed = 0.5f;
	private float lifetime = 2.0f; //in seconds.
	
	private int direction = 0;

	public Bullet(String name, int direction, float x, float y) throws SlickException{
		super(name);
		
		Image img = AssetManager.IMAGE_BULLET;
		this.setImage(img);
		
		boundingBox = new Rectangle(x, y, AssetManager.IMAGE_BULLET.getWidth(), AssetManager.IMAGE_BULLET.getHeight());
		
		this.direction = direction;
		this.x = x;
		this.y = y;
	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, SpriteManager manager, int delta) throws SlickException {
		if (direction == 0) {
			y -= speed * delta;
		} else if (direction == 1) {
			x -= speed * delta;
		} else if (direction == 2) {
			y += speed * delta;
		} else if (direction == 3) {
			x += speed * delta;
		}
		
		boundingBox.setLocation(x, y);
		
		//System.out.println(delta);
		
		lifetime -= 0.001f * delta;
		if (lifetime <= 0.0f) {
			manager.removeSprite(this);
		}
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, SpriteManager manager, Graphics g) throws SlickException {
		g.drawImage(getImage(), x, y);
	}

}
