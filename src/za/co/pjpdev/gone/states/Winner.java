package za.co.pjpdev.gone.states;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import za.co.pjpdev.gone.AssetManager;
import za.co.pjpdev.gone.Main;

public class Winner extends BasicGameState {
	
	private int id = -1;
	
	private Image background;

	public Winner(int id) {
		this.id = id;
	}

	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		/* Init */
		background = AssetManager.loadImage("winner_back.png");
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		/* Render */
		g.drawImage(background, 0, 0);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		/* Update */
		
		Input in = container.getInput();
		
		//in.clearKeyPressedRecord();
		
		if (in.isKeyPressed(Input.KEY_X)) {
			//game.enterState(Main.STATE_GAMEPLAY);
		}
		
		if (in.isKeyPressed(Input.KEY_ESCAPE)) {
			container.exit();
		}
	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return id;
	}

}
