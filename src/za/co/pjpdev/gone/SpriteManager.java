package za.co.pjpdev.gone;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Vector;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import za.co.pjpdev.gone.sprites.Sprite;

public class SpriteManager {
	
	private ArrayList<Sprite> sprites;
	private ArrayList<Sprite> sprites_2;
	
	private boolean updated = false;
	
	/**
	 * SpriteManager
	 * Update and render sprite entities.
	 * @author Peter-John
	 */

	public SpriteManager() {
		/* Create the manager */
		sprites = new ArrayList<Sprite>();
		sprites_2 = new ArrayList<Sprite>();
	}
	
	public void clear() {
		sprites.clear();
		updated = false;
	}
	
	public void addSprite(Sprite sprite) {
		/* Add a sprite to the list */
		if (!updated) {
			sprites.add(sprite);
		} else {
			sprites_2 = (ArrayList<Sprite>) sprites.clone();
			sprites_2.add(sprite);
		}
	}
	
	public void removeSprite(String name) {
		for (Sprite sprite : sprites) {
			if (sprite.getName().equals(name)) {
				removeSprite(sprite);
			}
		}
	}
	
	public Sprite getSprite(String name) {
		Sprite sprite = null;
		
		for (int i = 0; i < sprites.size(); i++) {
			if (sprites.get(i).getName().equals(name)) {
				sprite = sprites.get(i);
			}
		}
		
		return sprite;
	}
	
	public ArrayList<Sprite> getSprites() {
		return sprites;
	}
	
	public void removeSprite(Sprite sprite) {
		sprites_2 = (ArrayList<Sprite>) sprites.clone();
		sprites_2.remove(sprite);
	}
	
	public void update(GameContainer gc, StateBasedGame game, int delta) throws SlickException {
		/* Update enabled sprites in the list */
		updated = true;
		
		for (ListIterator<Sprite> itr = sprites.listIterator(); itr.hasNext();) {
			Sprite spr = itr.next();
			if (spr.isEnabled())
				spr.update(gc, game, this, delta);
		}
		
		if (!sprites_2.isEmpty()) {
			sprites = (ArrayList<Sprite>) sprites_2.clone();
			sprites_2.clear();
		}
	}
	
	public void render(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException {
		/* Render enabled sprites in the list */
		for (ListIterator<Sprite> itr = sprites.listIterator(); itr.hasNext();) {
			Sprite spr = itr.next();
			if (spr.isEnabled())
				spr.render(gc, game, this, g);
		}
	}

}
