package za.co.pjpdev.gone.sprites;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;

import za.co.pjpdev.gone.SpriteManager;

public abstract class Sprite {
	
	private String name;
	private boolean enabled = true;
	
	private Image image;
	
	protected Rectangle boundingBox;
	
	public float x = 0;
	public float y = 0;
		
	/**
	 * Sprite
	 * Base sprite abstract class
	 */
	public Sprite(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public void setImage(Image image) {
		this.image = image;
	}
	
	public Image getImage() {
		return image;
	}
	
	public abstract void update(GameContainer gc, StateBasedGame game, SpriteManager manager, int delta) throws SlickException;
	public abstract void render(GameContainer gc, StateBasedGame game, SpriteManager manager, Graphics g) throws SlickException;
}
