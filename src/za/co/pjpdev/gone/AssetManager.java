package za.co.pjpdev.gone;

import org.newdawn.slick.Image;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

/**
 * AssetManager
 * Easy way to load assets from the game.
 */

public class AssetManager {

	public static final String ASSETS_DIR = "./assets/";
	
	/* Images */
	public static final Image IMAGE_PLAYER;
	public static final Image IMAGE_BULLET;
	public static final Image IMAGE_WALL01;
	public static final Image IMAGE_FLOOR01;
	public static final Image IMAGE_DOOR;
	public static final Image IMAGE_INTEL;
	public static final Image IMAGE_ESCAPE;
	static {
		Image player;
		Image bullet;
		Image wall01;
		Image floor01;
		Image door;
		Image intel;
		Image escape;
		try {
			player = loadImage("player.png");
			bullet = loadImage("bullet.png");
			wall01 = loadImage("wall_01.png");
			floor01 = loadImage("floor_01.png");
			door = loadImage("door.png");
			intel = loadImage("intel.png");
			escape = loadImage("escape.png");
		} catch (SlickException ex) {
			player = null;
			bullet = null;
			wall01 = null;
			floor01 = null;
			door = null;
			intel = null;
			escape = null;
		}
		IMAGE_PLAYER = player;
		IMAGE_BULLET = bullet;
		IMAGE_WALL01 = wall01;
		IMAGE_FLOOR01 = floor01;
		IMAGE_DOOR = door;
		IMAGE_INTEL = intel;
		IMAGE_ESCAPE = escape;
	}
	
	public static Image loadImage(String key) throws SlickException {
		/* Loads an Image from the pool */
		Image img = new Image(ASSETS_DIR + "gfx/" + key);
		
		return img;
	}
	
	public static Sound loadSound(String name) throws SlickException {
		/* Loads a sound from the asset pool */
		Sound sound = new Sound(ASSETS_DIR + "audio/" + name);
		
		return sound;
	}
	
	public static Music loadMusic(String name) throws SlickException {
		Music music = new Music(ASSETS_DIR + "audio/" + name);
		
		return music;
	}

}
