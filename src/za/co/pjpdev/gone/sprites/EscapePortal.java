package za.co.pjpdev.gone.sprites;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;

import za.co.pjpdev.gone.SpriteManager;

public class EscapePortal extends Sprite {

	public EscapePortal(String name) {
		super(name);
		
		boundingBox = new Rectangle(x, y, 64, 64);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, SpriteManager manager, int delta) throws SlickException {
		/* Update */
		boundingBox.setLocation(x, y);
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, SpriteManager manager, Graphics g) throws SlickException {
		/* Render */
	}

}
