package za.co.pjpdev.gone.states;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import za.co.pjpdev.gone.AssetManager;
import za.co.pjpdev.gone.Main;

public class MenuState extends BasicGameState {
	
	private int id = -1;
	
	/* Menu elements */
	private Image background;

	public MenuState(int id) {
		this.id = id;
	}

	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		/* Initialise the state and load assets */
		background = AssetManager.loadImage("menu_back.png");
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		/* Render some stuff... */
		g.drawImage(background, 0, 0);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		/* Input wrangling */
		Input in = container.getInput();
		
		if (in.isKeyDown(Input.KEY_X)) {
			/* Start game */
			game.enterState(Main.STATE_GAMEPLAY);
		}
		
		if (in.isKeyDown(Input.KEY_ESCAPE)) {
			/* Quit the game, goodbye */
			container.exit();
		}
	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return id;
	}

}
