package za.co.pjpdev.gone.sprites;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;

import za.co.pjpdev.gone.AssetManager;
import za.co.pjpdev.gone.SpriteManager;

public class Intel extends Sprite {
	
	public static final float MAX_VALUE = 5.0f;
	
	public float value = 5.0f;
	
	private boolean stolen = false;

	public Intel(String name) {
		super(name);
		
		setImage(AssetManager.IMAGE_INTEL);
		boundingBox = new Rectangle(x, y, AssetManager.IMAGE_INTEL.getWidth(), AssetManager.IMAGE_INTEL.getHeight());
	}
	
	public boolean isStolen() {
		return stolen;
	}
	
	public void reset() {
		stolen = false;
		value = 5.0f;
		setEnabled(true);
		boundingBox.setSize(AssetManager.IMAGE_INTEL.getWidth(), AssetManager.IMAGE_INTEL.getHeight());
	}
	
	@Override
	public void update(GameContainer gc, StateBasedGame game, SpriteManager manager, int delta) throws SlickException {
		/* Update */
		boundingBox.setLocation(x, y);
		
		if ((value < MAX_VALUE) && (value > 0.0f)) {
			value += 0.0001f * delta;
		} else if (value < 0.0f) {
			stolen = true;
			boundingBox.setSize(0, 0);
			setEnabled(false);
		}
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, SpriteManager manager, Graphics g) throws SlickException {
		/* Render */
		g.drawImage(getImage(), x, y);
		
		g.draw(boundingBox);
	}

}
